//
//  AppDelegate.h
//  SourceSample
//
//  Created by Christopher Nelson on 11/22/16.
//  Copyright © 2016 Anderson Merchandisers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

